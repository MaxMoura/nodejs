// fs = file system
let fs = require('fs');

/**=== Função para criar um arquivo ===*/
// fs.writeFile('data.txt', 'Helllo word from SON', function (err) {
//     if (err) {
//         throw err;
//     }
// });

/**=== Função para lê um arquivo ===*/
// fs
//     .readFile('data.txt', function (err, data) {
//         if (err) {
//             throw err;
//         }
//
//         console.log(data.toString('utf8'));
//     });


/**=== Função para buscar um arquivo dentro de um diretório ===*/
// fs
//     .readdir('.', function (err, files) {
// //         if (err) {
// //             throw err;
// //         }
// //
// //         for (let file in files) {
// //             console.log(files[file]);
// //         }
//     });

/**=== Função para lê um arquivo de forma assicrona ===*/
fs
    .readdirSync('.')
    .filter(function (file) {
        return (file.endsWith('.js'))
    })
    .forEach(function (file) {
        console.log(file)
    })