let express = require('express');

let app = express();

app.get('/', function (req, res) {
    res.send('Hello from express SON');
});

app.get('/helo/:name', function (req, res) {
    res.json({
        message: 'Isso é um json Rsrsr com paramentro ' + req.params.name
    })
});

app.get('/hello', function (req, res) {
    res.json({
        message: 'This is my router hello'
    })
});

app.listen(3000, function () {
    console.log('express has been started');
});