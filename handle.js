let querystring = require('querystring');

function handle(request, response) {
    response.writeHead(200, {
        'Content-Type': 'text/html'
    });

    let body = '';

    request.on('data', function (data) {
        body += data.toString();
    });

    request.on('end', function () {
        let decode = querystring.parse(body);

        console.log(decode);
    });

    response.write('<!DOCTYPE "html">');
    response.write('<html>');
    response.write('<head><title>Max Node</title></head>');
    response.write('<body>');
    response.write('<header>');
    response.write('<meta charset="utf-8">');
    response.write('</header>');
    response.write('<h1>Olá! Sou o Max Moura</h1>');
    response.write('</body>');
    response.write('</html>');

    response.end();
}

exports.fn = handle;

/**=== Isso se chama modoulização ===*/
//module.exports = handle;